Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0022-flip-shuttle-unit-fsu).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |                  |
| assembly drawing         |  [de](https://gitlab.com/ourplant.net/products/s3-0022-flip-shuttle-unit-fsu/-/raw/main/02_assembly_drawing/s3-0022-a_flip_shuttle_unit_fsu.PDF)                 |
| circuit diagram          |   [de](https://gitlab.com/ourplant.net/products/s3-0022-flip-shuttle-unit-fsu/-/raw/main/03_circuit_diagram/S3-0022-EPLAN-A.pdf)                   |
| maintenance instructions |                  |
| spare parts              |   [de](https://gitlab.com/ourplant.net/products/s3-0022-flip-shuttle-unit-fsu/-/raw/main/05_spare_parts/S3-0022-EVL-A.pdf)                   |

